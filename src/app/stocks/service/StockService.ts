import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Stock} from '../model/Stock';
import {environment} from '../../../environments/environment';
import {StockTable} from '../model/StockTable';
import {StockRepository} from '../model/StockRepository';

@Injectable({
  providedIn: 'root'
})
export class StockService {

  convertToStockTable(data: Stock[]): StockTable[] {
    const result: StockTable[] = [];
    for (const row of data) {
      result.push(
        {
          name: {val: row.name},
          purchaseLimit: {val: row.purchaseLimit, color: null},
          rate: {val: row.rate, color: null},
          saleLimit: {val: row.saleLimit, color: null}
        }
      );
    }
    return result;
  }

  findByNameStockTable(data: StockTable[], name: string): StockTable {
    for (const row of data) {
      if (row.name.val === name) {
        return row;
      }
    }
    return null;
  }

  updateFields(data: StockTable[], stock: Stock): void {
    const stockVal = this.findByNameStockTable(data, stock.name);
    console.log(stockVal);
    if (stockVal.rate.val !== stock.rate) {
      if (stock.rate > stockVal.rate.val) {
        stockVal.rate.color = 'red';
      } else {
        stockVal.rate.color = 'lime';
      }
      stockVal.rate.val = stock.rate;
    }
    if (stockVal.purchaseLimit.val !== stock.purchaseLimit) {
      if (stock.purchaseLimit > stockVal.purchaseLimit.val) {
        stockVal.purchaseLimit.color = 'red';
      } else {
        stockVal.purchaseLimit.color = 'lime';
      }
      stockVal.purchaseLimit.val = stock.purchaseLimit;
    }
    if (stockVal.saleLimit.val !== stock.saleLimit) {
      if (stock.saleLimit > stockVal.saleLimit.val) {
        stockVal.saleLimit.color = 'red';
      } else {
        stockVal.saleLimit.color = 'lime';
      }
      stockVal.saleLimit.val = stock.saleLimit;
    }
  }
}
