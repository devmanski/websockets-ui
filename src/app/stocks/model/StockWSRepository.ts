import {Injectable} from '@angular/core';
import {RxStomp} from '@stomp/rx-stomp';
import * as SockJS from 'sockjs-client';
import {map} from 'rxjs/operators';
import {environment} from '../../../environments/environment';
import {Stock} from './Stock';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StockWSRepository {
  private client: RxStomp;
  constructor() {
    this.client = new RxStomp();
  }

  private setup() {
      this.client.configure({
        webSocketFactory: () => new SockJS(`${environment.url}/ws/stocks`)
      });
      this.client.activate();
      this.client.publish({destination: '/app/start'});
  }

  watchForStocks(): Observable<Stock> {
    this.setup();
    return this.client.watch('/user/topic/stocks')
      .pipe(
        map((response) => {
          const text: Stock = JSON.parse(response.body);
          return text;
        }));
  }

  private disconnectClicked() {
    if (this.client && this.client.connected) {
      this.client.deactivate();
      this.client = null;
    }
  }
}
