export interface Stock {
  name: string;
  purchaseLimit: number;
  saleLimit: number;
  rate: number;
}
