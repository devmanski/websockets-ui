export interface StockTable {
  name: { val: string };
  purchaseLimit: { val: number, color: string };
  saleLimit: { val: number, color: string };
  rate: { val: number, color: string };
}
