import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Stock} from './Stock';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class StockRepository {

  constructor(private http: HttpClient) {
  }

  getStocks(): Observable<Stock[]> {
    return this.http.get<Stock[]>(`${environment.url}/stocks`);
  }

}
