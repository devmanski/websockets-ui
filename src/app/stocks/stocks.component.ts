import {Component, OnInit, ViewChild} from '@angular/core';
import {MatSort, MatTableDataSource} from '@angular/material';
import {StockTable} from './model/StockTable';
import {StockRepository} from './model/StockRepository';
import {Stock} from './model/Stock';
import {StockService} from './service/StockService';
import {StockWSRepository} from './model/StockWSRepository';

@Component({
  selector: 'app-stocks',
  templateUrl: './stocks.component.html',
  styleUrls: ['./stocks.component.css']
})
export class StocksComponent implements OnInit {
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  dataSource = new MatTableDataSource<StockTable>();
  displayColumns: string[] = ['name', 'purchaseLimit', 'saleLimit', 'rate'];

  constructor(private stockRepository: StockRepository
    , private stockService: StockService, private stockwsrepository: StockWSRepository
  ) {
  }

  ngOnInit() {
    this.dataSource.sort = this.sort;
    this.stockRepository
      .getStocks()
      .subscribe(
        (data: Stock[]) => {
          this.dataSource.data = this.stockService.convertToStockTable(data);
          this.stockwsrepository.watchForStocks()
            .subscribe((data2: Stock) => {
              this.stockService.updateFields(this.dataSource.data, data2);
            });
        }
      );
  }

  click(): void {
    this.dataSource.data[4].rate.color = 'red';
  }
}
