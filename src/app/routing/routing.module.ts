import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {StocksComponent} from '../stocks/stocks.component';

const routes: Routes = [
  { path: 'stocks', component: StocksComponent},
  { path: '', redirectTo: '/stocks', pathMatch: 'full' }

];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ]
  ,
  exports: [
    RouterModule
  ]
})
export class RoutingModule { }
